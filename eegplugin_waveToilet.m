% eegplugin_Wavetoilet(): A plugin for EEGLAB to perform group-level analysis
%                         using independent components (ICs).
%
% Author: Makoto Miyakoshi, SCCN,INC,UCSD
%
% History:
% 12/18/2020 Makoto. As Robert Oostenveld recommmended to reconsider the code name 'Fieldstrip', I changed it to 'Wavetoilet'.  
% 11/29/2020 Makoto. Created.

% Copyright (C) 2020, Makoto Miyakoshi (mmiyakoshi@ucsd.edu) , SCCN,INC,UCSD
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
% THE POSSIBILITY OF SUCH DAMAGE.

function vers = eegplugin_Wavetoilet(fig, try_strings, catch_strings)

vers = '0.01';

if nargin < 3
    error('eegplugin_Wavetoilet requires 3 arguments');
end;

% Create the highLevelMenu.
highLevelMenu = findobj(fig, 'tag', 'tools');
set(highLevelMenu, 'UserData', 'startup:on;study:on'); % This unlocks 'Tools' menu without loading .set data haha.
submenu       = uimenu(highLevelMenu, 'label', 'Wavetoilet','separator','on');

% Add submenus.
uimenu( submenu, 'label', '1.IC selection',               'callback', 'Wavetoilet_icSelection');
uimenu( submenu, 'label', '2.Precompute',                 'callback', 'Wavetoilet_precompute');
uimenu( submenu, 'label', '3.Structure group-level data', 'callback', 'Wavetoilet_structureGroupLevelData');
uimenu( submenu, 'label', '4.Cluster ICs',                'callback', 'Wavetoilet_clusterIcs');
uimenu( submenu, 'label', '5.Perform statistics',         'callback', 'Wavetoilet_performStatistics');
uimenu( submenu, 'label', '6.Data visualization',         'callback', 'Wavetoilet_dataVisualization');
uimenu( submenu, 'label', '7.Exporting results',          'callback', 'Wavetoilet_exportingResults');