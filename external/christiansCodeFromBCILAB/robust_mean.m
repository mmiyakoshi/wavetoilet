function X = robust_mean(Y,rho,iters)
% https://github.com/sccn/BCILAB/blob/master/code/misc/robust_mean.m
% Perform a robust mean under the Huber loss function.
% x = robust_mean(Y,rho,iters)
%
% Input:
%   Y : MxN matrix over which to average (columnwise)
%   rho : augmented Lagrangian variable (default: 1)
%         smaller, more outlier suppression. (12/12/2020 Makoto)
%   iters : number of iterations to perform (default: 1000)
%           larger, more outlier suppression. (12/12/2020 Makoto)
%           Taken from BCILAB flt_reref() comment
%           'Iterations for huber fitting. A larger number yields tolerance
%           to larger outliers.'
%
% Output:
%   x : 1xN vector that is the roust mean of Y
%
% Note (12/12/2020 Makoto):
%       Try the following snipet to evaluate the behavior of the output.
%       Iteration 100 torelates meanVal x 100 outlier.
%       Iteration 1000 torelates meanVal x 1600 outlier.
%       there are ten data points plus one outlier and rho = 1.
%                           
%     clear y1 y2 y3 y4 y5
%     for iterIdx = 1:5
%         for n = 1:16
%             testVar = [0:9 2^n]';
%             y1(n,iterIdx) = mean(testVar);
%             y2(n,iterIdx) = robust_mean(testVar,0.1, 10^iterIdx);
%             y3(n,iterIdx) = robust_mean(testVar,1,   10^iterIdx);
%             y4(n,iterIdx) = robust_mean(testVar,10,  10^iterIdx);
%             y5(n,iterIdx) = robust_mean(testVar,100, 10^iterIdx);
%         end
%     end
%
% Based on the ADMM Matlab codes also found at:
%   http://www.stanford.edu/~boyd/papers/distr_opt_stat_learning_admm.html
%
%                                Christian Kothe, Swartz Center for Computational Neuroscience, UCSD
%                                2013-09-26

if ~exist('rho','var')
    rho = 1; end
if ~exist('iters','var')
    iters = 1000; end

m = size(Y,1);
if m==1
    X = Y;
else
    mu = sum(Y)/m;
    Z = zeros(size(Y)); U = Z;
    for k = 1:iters
        X = mu + sum(Z - U)/m;
        D = bsxfun(@minus,X,Y - U);
        Z = (rho/(1+rho) + (1/(1+rho))*max(0,(1-(1+1/rho)./abs(D)))).*D;
        U = D - Z;
    end
end