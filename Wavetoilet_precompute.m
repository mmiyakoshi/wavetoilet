% Wavetoilet_precompute(varargin)
%
% History
% 12/18/2020 Makoto. Renamed to Wavetoilet.
% 12/09/2020 Makoto. Siyi Deng's super-fast wavelet transform implemented.
% 12/05/2020 Makoto. Updated.
% 11/23/2020 Makoto. Updated.
% 11/08/2020 Makoto. Created. Based on my development for Steve Wu's project.

% Copyright (C) 2020, Makoto Miyakoshi (mmiyakoshi@ucsd.edu) , SCCN,INC,UCSD
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
% THE POSSIBILITY OF SUCH DAMAGE.

function varargout = Wavetoilet_precompute(varargin)
% WAVETOILET_PRECOMPUTE MATLAB code for Wavetoilet_precompute.fig
%      WAVETOILET_PRECOMPUTE, by itself, creates a new WAVETOILET_PRECOMPUTE or raises the existing
%      singleton*.
%
%      H = WAVETOILET_PRECOMPUTE returns the handle to a new WAVETOILET_PRECOMPUTE or the handle to
%      the existing singleton*.
%
%      WAVETOILET_PRECOMPUTE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WAVETOILET_PRECOMPUTE.M with the given input arguments.
%
%      WAVETOILET_PRECOMPUTE('Property','Value',...) creates a new WAVETOILET_PRECOMPUTE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Wavetoilet_precompute_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Wavetoilet_precompute_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Wavetoilet_precompute

% Last Modified by GUIDE v2.5 18-Dec-2020 04:34:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Wavetoilet_precompute_OpeningFcn, ...
                   'gui_OutputFcn',  @Wavetoilet_precompute_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Wavetoilet_precompute is made visible.
function Wavetoilet_precompute_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Wavetoilet_precompute (see VARARGIN)

% Choose default command line output for Wavetoilet_precompute
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Wavetoilet_precompute wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Wavetoilet_precompute_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function freqRangeEdit_Callback(hObject, eventdata, handles)
% hObject    handle to freqRangeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freqRangeEdit as text
%        str2num(get(hObject,'String')) returns contents of freqRangeEdit as a double


% --- Executes during object creation, after setting all properties.
function freqRangeEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freqRangeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function severeRejectionEdit_Callback(hObject, eventdata, handles)
% hObject    handle to severeRejectionEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of severeRejectionEdit as text
%        str2num(get(hObject,'String')) returns contents of severeRejectionEdit as a double


% --- Executes during object creation, after setting all properties.
function severeRejectionEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to severeRejectionEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function eegEventEdit_Callback(hObject, eventdata, handles)
% hObject    handle to eegEventEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eegEventEdit as text
%        str2double(get(hObject,'String')) returns contents of eegEventEdit as a double


% --- Executes during object creation, after setting all properties.
function eegEventEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eegEventEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in latencyZeroEventListbox.
function latencyZeroEventListbox_Callback(hObject, eventdata, handles)
% hObject    handle to latencyZeroEventListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns latencyZeroEventListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from latencyZeroEventListbox


% --- Executes during object creation, after setting all properties.
function latencyZeroEventListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to latencyZeroEventListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in baselineEventListbox.
function baselineEventListbox_Callback(hObject, eventdata, handles)
% hObject    handle to baselineEventListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns baselineEventListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from baselineEventListbox


% --- Executes during object creation, after setting all properties.
function baselineEventListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to baselineEventListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function epochEdgeEdit_Callback(hObject, eventdata, handles)
% hObject    handle to epochEdgeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of epochEdgeEdit as text
%        str2double(get(hObject,'String')) returns contents of epochEdgeEdit as a double


% --- Executes during object creation, after setting all properties.
function epochEdgeEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to epochEdgeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function baselineEdgeEdit_Callback(hObject, eventdata, handles)
% hObject    handle to baselineEdgeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of baselineEdgeEdit as text
%        str2double(get(hObject,'String')) returns contents of baselineEdgeEdit as a double


% --- Executes during object creation, after setting all properties.
function baselineEdgeEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to baselineEdgeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in selectSetFilesPushbutton.
function selectSetFilesPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to selectSetFilesPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Obtain multiple .set files
[allFiles, workingFolder] = uigetfile('*.set', 'MultiSelect', 'on');
if ~any(workingFolder)
    disp('Cancelled.')
    return
end

% Convert char to cell if n = 1.
if ischar(allFiles)
    subjName = allFiles;
    clear allFiles
    allFiles{1,1} = subjName;    
end

fieldName = get(handles.eegEventEdit, 'string');
allUniqueEventsStack = [];
numIcsList     = zeros(length(allFiles),3);
dataLengthList = zeros(length(allFiles),1);
for setIdx = 1:length(allFiles)
    loadName = allFiles{setIdx};
    dataName = loadName(1:end-4);
    
    %% STEP1: Load data.
    EEG = pop_loadset('filename', loadName, 'filepath', workingFolder, 'loadmode', 'info');

    %% STEP2: Obtain event labels.
    evalc(['allEvents = {EEG.event.' fieldName '}']);
    
    allUniqueEventsStack = [allUniqueEventsStack unique(allEvents)];
    
    %% STEP3: Obtain number of ICs and data legnth.
    numIcsList(setIdx,:)   = [size(EEG.icaweights,1) length(EEG.etc.Wavetoilet.nonartifactIcIdx) length(EEG.etc.Wavetoilet.brainIcIdx)];
    dataLengthList(setIdx) = EEG.xmax;
end
allUniqueEventsStack = unique(allUniqueEventsStack);

% Update the list boxes.
set(handles.latencyZeroEventListbox, 'String', allUniqueEventsStack)
set(handles.baselineEventListbox,    'String', allUniqueEventsStack)

% Attach the loaded data to an object.
uigetfileOutput.allFiles       = allFiles;
uigetfileOutput.workingFolder  = workingFolder;
uigetfileOutput.numIcsList     = numIcsList;
uigetfileOutput.dataLengthList = dataLengthList;
set(handles.selectSetFilesPushbutton, 'UserData', uigetfileOutput)

% % Choose default command line output for pop_groupSIFT_viewResultsAndExportForMovie
% handles.output = hObject;
% 
% % Update handles structure
% guidata(hObject, handles);



function numFreqBinsEdit_Callback(hObject, eventdata, handles)
% hObject    handle to numFreqBinsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numFreqBinsEdit as text
%        str2double(get(hObject,'String')) returns contents of numFreqBinsEdit as a double


% --- Executes during object creation, after setting all properties.
function numFreqBinsEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numFreqBinsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cycleDupletEdit_Callback(hObject, eventdata, handles)
% hObject    handle to cycleDupletEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cycleDupletEdit as text
%        str2double(get(hObject,'String')) returns contents of cycleDupletEdit as a double


% --- Executes during object creation, after setting all properties.
function cycleDupletEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cycleDupletEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function slidingWindowStepEdit_Callback(hObject, eventdata, handles)
% hObject    handle to slidingWindowStepEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of slidingWindowStepEdit as text
%        str2double(get(hObject,'String')) returns contents of slidingWindowStepEdit as a double


% --- Executes during object creation, after setting all properties.
function slidingWindowStepEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slidingWindowStepEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in icSelectionPopupmenu.
function icSelectionPopupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to icSelectionPopupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns icSelectionPopupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from icSelectionPopupmenu


% --- Executes during object creation, after setting all properties.
function icSelectionPopupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to icSelectionPopupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in startPushbutton.
function startPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to startPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Add path to Siyi's wavelet functions.
fullpathToThisFile = mfilename('fullpath');
pathToWaveletFunctions = [fullpathToThisFile(1:end-21) 'external' filesep 'siyisCodeFromRamesh'];
addpath(pathToWaveletFunctions);
existFcwt = exist('fcwt');
if existFcwt==0
    error('Please check the path to [eeglabroot]/plugins/Wavetoilet/external/siyisCodeFromRamesh')
end

    
% Retrieve the loaded file names.
uigetfileOutput = get(handles.selectSetFilesPushbutton, 'UserData');
allFiles        = uigetfileOutput.allFiles;
workingFolder   = uigetfileOutput.workingFolder;
switch get(handles.icSelectionPopupmenu, 'value')
    case 1
        numIcsList = uigetfileOutput.numIcsList(:,1);
    case 2
        numIcsList = uigetfileOutput.numIcsList(:,2);
    case 3
        numIcsList = uigetfileOutput.numIcsList(:,3);
end
dataLengthList  = uigetfileOutput.dataLengthList;
lengthIcProduct = sum(numIcsList.*dataLengthList);

% Display process start
disp(sprintf('\n'))
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%% ''2.Precompute'' started. %%%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(sprintf('\n'))

% Move to the working folder
cd(workingFolder)

% Obtain user-input parameters.
icSelection   =         get(handles.icSelectionPopupmenu,  'Value');
epochEdges    = str2num(get(handles.epochEdgeEdit,         'String'));
baselineEdges = str2num(get(handles.baselineEdgeEdit,      'String'));
freqRange     = str2num(get(handles.freqRangeEdit,         'String'));
numFreqBins   = str2num(get(handles.numFreqBinsEdit,       'String'));

% Determine the freq bins (Nattapong's method)
deviationFromLog = 5;
freqBins = logspace(log10(freqRange(1)+deviationFromLog), log10(freqRange(2)+deviationFromLog), numFreqBins)-deviationFromLog;

% Obtain user-input events.
allUniqueEventList = get(handles.latencyZeroEventListbox, 'String');
latencyZeroEvents  = allUniqueEventList(get(handles.latencyZeroEventListbox, 'value'));
baselineEvents     = allUniqueEventList(get(handles.baselineEventListbox,    'value'));

% Start the batch
numOriginalIcs      = zeros(length(allFiles),1);
numNonartifactIcs   = zeros(length(allFiles),1);
numBrainIcs         = zeros(length(allFiles),1);
processingTimePerFrameList = zeros(sum(numIcsList),1);
dataLengthProcessed = 0;
totalIcIdx  = 0;
waitbarHandle = waitbar(0, 'Estimating the ramining processing time...');
set(waitbarHandle, 'Color', [0.66 0.76 1]);
drawnow
for setIdx = 1:length(allFiles)
    loadName = allFiles{setIdx};
    dataName = loadName(1:end-4);
    
    %% STEP1: Load data.
    EEG = pop_loadset('filename', loadName, 'filepath', workingFolder);

    %% STEP2: Obtain all events.
    %allEvents = {EEG.event.type}';
    fieldName = get(handles.eegEventEdit, 'string');
    evalc(['allEvents = {EEG.event.' fieldName '}']);
    boundaryIdx = find(strcmp({EEG.event.type}, 'boundary'));
    boundaryLatencyS    = [EEG.event(boundaryIdx).latency]*1/EEG.srate; % s.
    epochLengthInFrames = ceil((epochEdges(2)-epochEdges(1))*EEG.srate)+1; % +1 is to include latency 0.
    baseLengthInFrames  = ceil((baselineEdges(2)-baselineEdges(1))*EEG.srate)+1;
    
    %% STEP4: Select ICs to process.
    switch icSelection
        case 1
            selectedIcIdx = 1:size(EEG.icaact,1);
        case 2
            selectedIcIdx = EEG.etc.Wavetoilet.nonartifactIcIdx;
        case 3
            selectedIcIdx = EEG.etc.Wavetoilet.brainIcIdx;
    end
    
    %% STEP5: Obtain epoch edges in s.
    eventSelectingIdx   = find(contains(allEvents, latencyZeroEvents));
    latencyZeroLatencyS = [EEG.event(eventSelectingIdx).latency]*1/EEG.srate; % s.
    epochEdgeList       = [[latencyZeroLatencyS + epochEdges(1)]' [latencyZeroLatencyS + epochEdges(2)]'];
    eventList           = allEvents(eventSelectingIdx);
    
    %% STEP6: Obtain epoch indices for exclusion due to boundary.
    epochExclusionIdx = [];
    for exclusionIdx = 1:length(boundaryLatencyS)
        currentExclusionEventS = boundaryLatencyS(exclusionIdx);
        for epochIdx = 1:size(epochEdgeList,1)
            if epochEdgeList(epochIdx,1)<=currentExclusionEventS & epochEdgeList(epochIdx,2)>=currentExclusionEventS
                epochExclusionIdx = sort([epochExclusionIdx; epochIdx]);
            end
        end
    end
    epochEdgeList(epochExclusionIdx,:)  = [];
    eventList(epochExclusionIdx) = [];
    
    %% STEP7: Obtain baseline edges in s.
    baselineSelectingIdx = find(contains(allEvents, baselineEvents));
    baselineLatencyS     = [EEG.event(baselineSelectingIdx).latency]*1/EEG.srate; % s.
    baselineEdgeList     = [[baselineLatencyS + baselineEdges(1)]' [baselineLatencyS + baselineEdges(2)]'];
     
    %% STEP8: Obtain baseline indices for exclusion due to boundary.
    baseExclusionIdx = [];
    for exclusionIdx = 1:length(boundaryLatencyS)
        currentExclusionEventS = boundaryLatencyS(exclusionIdx);
        for epochIdx = 1:size(epochEdgeList,1)
            if epochEdgeList(epochIdx,1)<=currentExclusionEventS & epochEdgeList(epochIdx,2)>=currentExclusionEventS
                baseExclusionIdx = sort([baseExclusionIdx; epochIdx]);
            end
        end
    end
    baselineEdgeList(baseExclusionIdx,:) = [];
    
    %% STEP9: Obtain wavelet scales.
    scal = hztoscale(freqBins,'morlet',EEG.srate);
    
    %% STEP10: Enter the IC loop.
    for icIdx = 1:length(selectedIcIdx)
        
        % Measure the time lapse.
        tStart = tic;
        
        % Obtain the current icIdx.
        currentIcIdx = selectedIcIdx(icIdx);

        %% STEP9: Compute scalp topos.
        [~, scalpTopo] = topoplot(EEG.icawinv(:,currentIcIdx), EEG.chanlocs, 'noplot', 'on');
        EEG.etc.Wavetoilet.scalpTopo(icIdx,:,:) = single(scalpTopo);
        
        %%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Process peri-event onset data. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % STEP11: Compute Siyi Deng's fast wavelet told by Ramesh.
        coeffs = fcwt(EEG.icaact(currentIcIdx,:), scal, 'morlet')';

        %% STEP12: Generate empty tensors for epoched data (only when icIdx==1)
        if icIdx == 1
            wtCoeff_ic_freq_time_trials = single(nan(length(selectedIcIdx), length(freqBins), epochLengthInFrames, size(epochEdgeList,1)));
            erp_ic_time_trials          = single(nan(length(selectedIcIdx),                   epochLengthInFrames, size(epochEdgeList,1)));
        end
        
        %% STEP13: Epoch data.
        for epochIdx = 1:size(epochEdgeList,1)
            epochFrameIdx = find(EEG.times/1000 >= epochEdgeList(epochIdx,1) & EEG.times/1000 <= epochEdgeList(epochIdx,2));
            
            % Adjust +/-1 frame.
            if     length(epochFrameIdx) < epochLengthInFrames
                epochFrameIdx(end+1) = epochFrameIdx(end)+1;
            elseif length(epochFrameIdx) > epochLengthInFrames
                epochFrameIdx(end+1) = epochFrameIdx(end)-1;
            end
           
            % Leave NaN if cannot crop the full length.
            if length(epochFrameIdx) == epochLengthInFrames;
                wtCoeff_ic_freq_time_trials(icIdx,:,:,epochIdx) = coeffs(               :,epochFrameIdx);
                erp_ic_time_trials(         icIdx,:,  epochIdx) = EEG.icaact(currentIcIdx,epochFrameIdx);
            else
                error('This error is not patched yet!')
            end
        end
        
        %%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Process baseline data. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % STEP14: Generate empty tensors for baseline.
        if icIdx == 1
            wtCoeffBase_ic_freq_time_trials = single(nan(length(selectedIcIdx), length(freqBins), baseLengthInFrames, size(baselineEdgeList,1)));
            erpBase_ic_time_trials          = single(nan(length(selectedIcIdx),                   baseLengthInFrames, size(baselineEdgeList,1)));
        end
        
        %% STEP15: Epoch ERP baseline window.
        for baselineIdx = 1:size(baselineEdgeList,1)
            baselineFrameIdx = find(EEG.times/1000 >= baselineEdgeList(baselineIdx,1) & EEG.times/1000 <= baselineEdgeList(baselineIdx,2));
            
            % Adjust +/-1 frame.
            if     length(baselineFrameIdx) < baseLengthInFrames
                baselineFrameIdx(end+1) = baselineFrameIdx(end)+1;
            elseif length(baselineFrameIdx) > baseLengthInFrames
                baselineFrameIdx(end+1) = baselineFrameIdx(end)-1;
            end
            
            % Leave NaN if cannot crop the full length.
            if length(baselineFrameIdx) == baseLengthInFrames
                wtCoeffBase_ic_freq_time_trials(icIdx,:,:,baselineIdx) = coeffs(               :,baselineFrameIdx);
                erpBase_ic_time_trials(         icIdx,:,  baselineIdx) = EEG.icaact(currentIcIdx,baselineFrameIdx);
            else
                error('This error is not patched yet!')
            end
        end

        %% STEP16: Estimate processing time.
        tElapsed = toc(tStart);
        totalIcIdx = totalIcIdx +1;
        currentIcProcessingTimePerFrame = tElapsed/dataLengthList(setIdx); % s
        processingTimePerFrameList(totalIcIdx) = currentIcProcessingTimePerFrame;
        averageSpeed = mean(processingTimePerFrameList(1:totalIcIdx));
        dataLengthProcessed = dataLengthProcessed + dataLengthList(setIdx);
        remainingDataLength = lengthIcProduct-dataLengthProcessed;
        remainingTime = remainingDataLength*averageSpeed;
        percentFinished = 100*dataLengthProcessed/lengthIcProduct;
        if     remainingTime >= 86400
            waitbarMessage = sprintf('\n\n%.0f%% done. Remaining %.1f days.',    percentFinished, remainingTime/86400);
        elseif remainingTime >= 3600
            waitbarMessage = sprintf('\n\n%.0f%% done. Remaining %.1f hours.',   percentFinished, remainingTime/3600);
        else
            waitbarMessage = sprintf('\n\n%.0f%% done. Remaining %.1f minutes.', percentFinished, remainingTime/60);
        end
        waitbarHandle = waitbar(percentFinished/100, waitbarHandle, waitbarMessage);
%         colorbarHandle = findobj(waitbarHandle,'Type','Patch');
%         set(colorbarHandle, 'EdgeColor',[0 0 0], 'FaceColor',[1 0.66 0.76])
        set(waitbarHandle, 'Color', [0.66 0.76 1]);
        drawnow
    end

    %% STEP17: Stock the result to Wavetoilet structure.
    epochTime    = epochEdges(1):1/EEG.srate:epochEdges(2);
    baselineTime = baselineEdges(1):1/EEG.srate:baselineEdges(2);
    
    EEG.etc.Wavetoilet.eventList   = eventList;
    EEG.etc.Wavetoilet.icSelection = icSelection;
    EEG.etc.Wavetoilet.latencyZeroEvents = latencyZeroEvents;
    EEG.etc.Wavetoilet.baselineEvents    = baselineEvents;
    EEG.etc.Wavetoilet.epochTime         = epochTime;
    EEG.etc.Wavetoilet.baselineTime      = baselineTime;
    EEG.etc.Wavetoilet.wtFreqs     = freqBins;
    EEG.etc.Wavetoilet.wtType      = 'Morlet';
    EEG.etc.Wavetoilet.wtCoeff     = wtCoeff_ic_freq_time_trials;
    EEG.etc.Wavetoilet.wtCoeffBase = wtCoeffBase_ic_freq_time_trials;
    EEG.etc.Wavetoilet.wtCoeffDims = {'IC' 'Freq' 'Time' 'Trials'};
    EEG.etc.Wavetoilet.erp         = erp_ic_time_trials;
    EEG.etc.Wavetoilet.erpDims     = {'IC' 'Time' 'Trials'};
    EEG.etc.Wavetoilet.erpBase     = erpBase_ic_time_trials;

    %% STEP18: Save data
    pop_saveset(EEG, 'filename', dataName, 'filepath', workingFolder, 'version', '7.3');
end
close(waitbarHandle)

% Display process end
disp(sprintf('\n'))
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%% ''2.Precompute'' finished. %%%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(sprintf('\n'))
