% Wavetoilet_clusterIcs(varargin)
%
% History
% 12/18/2020 Makoto. Renamed to Wavetoilet.
% 12/09/2020 Makoto. Created.

% Copyright (C) 2020, Makoto Miyakoshi (mmiyakoshi@ucsd.edu) , SCCN,INC,UCSD
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
% THE POSSIBILITY OF SUCH DAMAGE.

function varargout = Wavetoilet_clusterIcs(varargin)
% WAVETOILET_CLUSTERICS MATLAB code for Wavetoilet_clusterIcs.fig
%      WAVETOILET_CLUSTERICS, by itself, creates a new WAVETOILET_CLUSTERICS or raises the existing
%      singleton*.
%
%      H = WAVETOILET_CLUSTERICS returns the handle to a new WAVETOILET_CLUSTERICS or the handle to
%      the existing singleton*.
%
%      WAVETOILET_CLUSTERICS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WAVETOILET_CLUSTERICS.M with the given input arguments.
%
%      WAVETOILET_CLUSTERICS('Property','Value',...) creates a new WAVETOILET_CLUSTERICS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Wavetoilet_clusterIcs_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Wavetoilet_clusterIcs_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Wavetoilet_clusterIcs

% Last Modified by GUIDE v2.5 18-Dec-2020 04:36:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Wavetoilet_clusterIcs_OpeningFcn, ...
                   'gui_OutputFcn',  @Wavetoilet_clusterIcs_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Wavetoilet_clusterIcs is made visible.
function Wavetoilet_clusterIcs_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Wavetoilet_clusterIcs (see VARARGIN)

% Choose default command line output for Wavetoilet_clusterIcs
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Wavetoilet_clusterIcs wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Wavetoilet_clusterIcs_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadPushbutton.
function loadPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to loadPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

disp(sprintf('\n'))
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%% ''4. Cluster ICs'' started. %%%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(sprintf('\n'))

% Obtain multiple .set files
[fileName, pathName] = uigetfile('*_Wavetoilet1st.mat', 'MultiSelect', 'off');
if ~any(fileName)
    disp('Cancelled.')
    return
end

% Load the 1st group-level data.
% loadingFile = get(handles.loadEdit, 'String');
load([pathName fileName])
disp('Group-level data structure (_Wavetoilet1st.mat) loaded successfully.')
disp('Splitting the bilateral symmetric dipoles.')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Split bilateral dipoles. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Enter the for loop.
dualDipoleCount = 0;
for subjIdx = 1:length(Wavetoilet.subjects)
    
    % Output of length(Wavetoilet.subjects(subjIdx).dipfitModels
    % dynamically changes. forloop cannot be used in this case, but while
    % loop worked. (12/16/2020 Makoto)
    %
    % for icIdx = 1:length(Wavetoilet.subjects(subjIdx).dipfitModels)
    icIdx = 0;
    while icIdx < length(Wavetoilet.subjects(subjIdx).dipfitModels)
        icIdx = icIdx + 1;
        
        if size(Wavetoilet.subjects(subjIdx).dipfitModels(icIdx).posxyz,1) == 2
            dualDipoleCount = dualDipoleCount + 1;
            
            % Process the dipfit models.
            currentDipfitModels  = Wavetoilet.subjects(subjIdx).dipfitModels;
            modifiedDipfitModels = [currentDipfitModels(1:icIdx) currentDipfitModels(icIdx:end)];
            modifiedDipfitModels(icIdx).posxyz = modifiedDipfitModels(icIdx).posxyz(1,:);
            modifiedDipfitModels(icIdx).momxyz = modifiedDipfitModels(icIdx).momxyz(1,:);
            modifiedDipfitModels(icIdx+1).posxyz = modifiedDipfitModels(icIdx+1).posxyz(2,:);
            modifiedDipfitModels(icIdx+1).momxyz = modifiedDipfitModels(icIdx+1).momxyz(2,:);
            Wavetoilet.subjects(subjIdx).dipfitModels = modifiedDipfitModels;
            
            % Process the icIdx.
            Wavetoilet.subjects(subjIdx).icIdx = [Wavetoilet.subjects(subjIdx).icIdx(1:icIdx); Wavetoilet.subjects(subjIdx).icIdx(icIdx:end)];
            
            % Process the scalpTopo.
            Wavetoilet.subjects(subjIdx).scalpTopo = cat(1, Wavetoilet.subjects(subjIdx).scalpTopo(1:icIdx,:,:), ...
                                                            Wavetoilet.subjects(subjIdx).scalpTopo(icIdx:end,:,:));
            % Process event data.
            for eventIdx = 1:length(Wavetoilet.subjects(subjIdx).event)
                
                % Process mean ERP.
                Wavetoilet.subjects(subjIdx).event(eventIdx).meanErp    = cat(1, Wavetoilet.subjects(subjIdx).event(eventIdx).meanErp(1:icIdx,:), ...
                                                                                 Wavetoilet.subjects(subjIdx).event(eventIdx).meanErp(icIdx:end,:));
                % Process robust ERP.
                Wavetoilet.subjects(subjIdx).event(eventIdx).robustErp  = cat(1, Wavetoilet.subjects(subjIdx).event(eventIdx).robustErp(1:icIdx,:), ...
                                                                                 Wavetoilet.subjects(subjIdx).event(eventIdx).robustErp(icIdx:end,:));
                % Process mean ERSP.
                Wavetoilet.subjects(subjIdx).event(eventIdx).meanErsp   = cat(1, Wavetoilet.subjects(subjIdx).event(eventIdx).meanErsp(1:icIdx,:,:), ...
                                                                                 Wavetoilet.subjects(subjIdx).event(eventIdx).meanErsp(icIdx:end,:,:)); 
                % Process robust ERSP.
                Wavetoilet.subjects(subjIdx).event(eventIdx).robustErsp = cat(1, Wavetoilet.subjects(subjIdx).event(eventIdx).robustErsp(1:icIdx,:,:), ...
                                                                                 Wavetoilet.subjects(subjIdx).event(eventIdx).robustErsp(icIdx:end,:,:));
                % Process ITC.
                Wavetoilet.subjects(subjIdx).event(eventIdx).itc        = cat(1, Wavetoilet.subjects(subjIdx).event(eventIdx).itc(1:icIdx,:,:), ...
                                                                                 Wavetoilet.subjects(subjIdx).event(eventIdx).itc(icIdx:end,:,:));
                % Process PSD.
                Wavetoilet.subjects(subjIdx).event(eventIdx).PSD        = cat(1, Wavetoilet.subjects(subjIdx).event(eventIdx).PSD(1:icIdx,:), ...
                                                                                 Wavetoilet.subjects(subjIdx).event(eventIdx).PSD(icIdx:end,:));
            end
        end
    end
end

% Obtain dipole posxyz list.
allPosxyz = zeros(1,5); % The first row is dummy.
for subjIdx = 1:length(Wavetoilet.subjects)
    posxyz_1D = [Wavetoilet.subjects(subjIdx).dipfitModels.posxyz];
    currentPosxyz = reshape(posxyz_1D, [3 length(posxyz_1D)/3])';
    allPosxyz = cat(1, allPosxyz, [repmat(subjIdx, [size(currentPosxyz,1),1]) [1:size(currentPosxyz,1)]' currentPosxyz]); % subjId, icIdx, x, y, z.
end
allPosxyz(1,:) = []; % Dummy data deleted.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Optimize the number of clusters between the range 5-20. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate 5-20 clustering results using kmeans.
maxNumClusters = 16;
kmeansClusters  = zeros(size(allPosxyz,1), maxNumClusters);
gmdistClusters  = zeros(size(allPosxyz,1), maxNumClusters);
options = statset('MaxIter',10000);
numClusters = 5:20;
for clustIdxIdx = 1:length(numClusters)
    
    currentNumClusters = numClusters(clustIdxIdx);
    
    % kmeans.
    kmeansClusters(:,clustIdxIdx) = kmeans(allPosxyz(:,3:5), currentNumClusters, 'emptyaction', 'singleton', 'maxiter', 10000, 'replicate', 100);
    
    % Gaussian mixture distribution model.
    try
        GMM = fitgmdist(allPosxyz(:,3:5), currentNumClusters, 'Replicates', 100, 'Options', options);
        gmdistClusters(:,clustIdxIdx) = cluster(GMM, allPosxyz(:,3:5));
    end
end
 
% Use Matlab function evalclusters().
evalClustSil_km = evalclusters(allPosxyz(:,3:5), kmeansClusters, 'Silhouette');
evalClustCH_km  = evalclusters(allPosxyz(:,3:5), kmeansClusters, 'CalinskiHarabasz');
evalClustDB_km  = evalclusters(allPosxyz(:,3:5), kmeansClusters, 'DaviesBouldin');

if any(sum(gmdistClusters,1)==0)
    disp('The number of ICs is too small to run fitgmdist(). Only kmeans result will be used.')
    
    % Plot the results.
    figure; set(gcf, 'color', [0.66 0.76 1])
    subplot(1,3,1)
    plot(evalClustSil_km);title('kmeans Silhouette');
    subplot(1,3,2)
    plot(evalClustDB_km); title('kmeans DaviesBouldin');
    subplot(1,3,3)
    plot(evalClustCH_km); title('kmeans CalinskiHarabasz');
    
    % Disable the algorithm selection (kmeans only).
    set(handles.methodPopupmenu, 'Enable', 'off')
else
    evalClustSil_gmm = evalclusters(allPosxyz(:,3:5), gmdistClusters, 'Silhouette');
    evalClustCH_gmm  = evalclusters(allPosxyz(:,3:5), gmdistClusters, 'CalinskiHarabasz');
    evalClustDB_gmm  = evalclusters(allPosxyz(:,3:5), gmdistClusters, 'DaviesBouldin');
    
    % Plot the results.
    figure; set(gcf, 'color', [0.66 0.76 1])
    subplot(2,3,1)
    plot(evalClustSil_km);title('kmeans Silhouette');
    subplot(2,3,2)
    plot(evalClustDB_km); title('kmeans DaviesBouldin');
    subplot(2,3,3)
    plot(evalClustCH_km); title('kmeans CalinskiHarabasz');
    subplot(2,3,4)
    plot(evalClustSil_gmm);title('GM Silhouette');
    subplot(2,3,5)
    plot(evalClustDB_gmm); title('GM DaviesBouldin');
    subplot(2,3,6)
    plot(evalClustCH_gmm); title('GM CalinskiHarabasz');
    
    % Enable the algorithm selection (kmeans or Gaussian mixture distribution model).
    set(handles.methodPopupmenu, 'Enable', 'off')
end
 
% Attach the calculated data to an object.
optimizationOutput.numClusters = 5:20;
optimizationOutput.kmeansClusters = kmeansClusters;
optimizationOutput.gmdistClusters = gmdistClusters;
optimizationOutput.allPosxyz      = allPosxyz;
optimizationOutput.Wavetoilet     = Wavetoilet;
set(handles.loadPushbutton, 'UserData', optimizationOutput)
disp('The results are stored under handles.loadPushbutton.UserData')



function loadEdit_Callback(hObject, eventdata, handles)
% hObject    handle to loadEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of loadEdit as text
%        str2double(get(hObject,'String')) returns contents of loadEdit as a double


% --- Executes during object creation, after setting all properties.
function loadEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to loadEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

 

% --- Executes on button press in performPushbutton.
function performPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to performPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Load the bilateral-dipole-split data.
optimizationOutput = get(handles.loadPushbutton, 'UserData');
Wavetoilet = optimizationOutput.Wavetoilet;

% Ontain user input.
clusteringMethod = get(handles.methodPopupmenu, 'Value'); % 1=kmeans, 2=GMM.
numClusters      = str2num(get(handles.numClustersEdit, 'String'));

% Find the cluster number index.
numClusterIdx = find(optimizationOutput.numClusters == numClusters);

% Obtain the precomputed clustering result.
switch clusteringMethod
    case 1
        icToClusterList = optimizationOutput.kmeansClusters(:,numClusterIdx);
    case 2
        icToClusterList = optimizationOutput.gmdistClusters(:,numClusterIdx);
end

% Concatenate the cluster index.
allPosxyz = optimizationOutput.allPosxyz;
allPosxyz = [allPosxyz(:,1:2) icToClusterList allPosxyz(:,3:end)]; % subjIdx, icIdx, clusterIdx, x, y, z.

% Populate the cluster number to Wavetoilet structure.
for subjIdx = 1:max(allPosxyz(:,1))
    currentSubjIdx = find(allPosxyz(:,1) == subjIdx);
    Wavetoilet.subjects(subjIdx).clusterIdx = icToClusterList(currentSubjIdx);
end

% Check event consistency across subjects.
allEvents = [];
for subjIdx = 1:max(allPosxyz(:,1))
    currentSubjEvents = [Wavetoilet.subjects(subjIdx).event.label];
    allEvents = [allEvents currentSubjEvents];
end
uniqueEvents = unique(allEvents);
for subjIdx = 1:max(allPosxyz(:,1))
    currentSubjEvents = [Wavetoilet.subjects(subjIdx).event.label];
    if sum(strcmp(currentSubjEvents, uniqueEvents)) ~= length(uniqueEvents);
        error('Event inconsistency detected! Aborted.')
    end
end
disp('Event consistency check passed.')

% Prepare data structure.
icCluster.epochTime = Wavetoilet.subjects(1).epochTime;
icCluster.wtFreqs   = Wavetoilet.subjects(1).wtFreqs;
icCluster.psdFreqs  = Wavetoilet.subjects(1).psdFreqs;
for clusterIdx = 1:max(icToClusterList)
    for eventIdx = 1:length(uniqueEvents)
        % Prepare the initial dummy data structure.
        icCluster(clusterIdx).event(eventIdx).group      = [];
        icCluster(clusterIdx).event(eventIdx).condition  = [];
        icCluster(clusterIdx).event(eventIdx).session    = [];
        
        icCluster(clusterIdx).event(eventIdx).meanErp    = [];
        icCluster(clusterIdx).event(eventIdx).robustErp  = [];
        icCluster(clusterIdx).event(eventIdx).meanErsp   = [];
        icCluster(clusterIdx).event(eventIdx).robustErsp = [];
        icCluster(clusterIdx).event(eventIdx).itc        = [];
        icCluster(clusterIdx).event(eventIdx).PSD        = [];
        icCluster(clusterIdx).event(eventIdx).subjIdx    = [];
        icCluster(clusterIdx).event(eventIdx).subjName   = [];
        icCluster(clusterIdx).event(eventIdx).icIdx      = [];
        icCluster(clusterIdx).event(eventIdx).scalpTopo  = [];
        icCluster(clusterIdx).event(eventIdx).posxyz     = [];
    end
end

% Populate the data.
for clusterIdx = 1:max(icToClusterList)
    for subjIdx = 1:max(allPosxyz(:,1))
        currentIcIdx = find(Wavetoilet.subjects(subjIdx).clusterIdx == clusterIdx);
        if isempty(currentIcIdx)
            continue
        else
            for eventIdx = 1:length(uniqueEvents)
                
                icCluster(clusterIdx).event(eventIdx).group      = cat(1, icCluster(clusterIdx).event(eventIdx).group,      Wavetoilet.subjects(subjIdx).group);
                icCluster(clusterIdx).event(eventIdx).condition  = cat(1, icCluster(clusterIdx).event(eventIdx).condition,  Wavetoilet.subjects(subjIdx).condition);
                icCluster(clusterIdx).event(eventIdx).session    = cat(1, icCluster(clusterIdx).event(eventIdx).session,    Wavetoilet.subjects(subjIdx).session);
                
                icCluster(clusterIdx).event(eventIdx).meanErp    = cat(1, icCluster(clusterIdx).event(eventIdx).meanErp,    Wavetoilet.subjects(subjIdx).event(eventIdx).meanErp(   currentIcIdx,:));
                icCluster(clusterIdx).event(eventIdx).robustErp  = cat(1, icCluster(clusterIdx).event(eventIdx).robustErp,  Wavetoilet.subjects(subjIdx).event(eventIdx).robustErp( currentIcIdx,:));
                icCluster(clusterIdx).event(eventIdx).meanErsp   = cat(1, icCluster(clusterIdx).event(eventIdx).meanErsp,   Wavetoilet.subjects(subjIdx).event(eventIdx).meanErsp(  currentIcIdx,:,:));
                icCluster(clusterIdx).event(eventIdx).robustErsp = cat(1, icCluster(clusterIdx).event(eventIdx).robustErsp, Wavetoilet.subjects(subjIdx).event(eventIdx).robustErsp(currentIcIdx,:,:));
                icCluster(clusterIdx).event(eventIdx).itc        = cat(1, icCluster(clusterIdx).event(eventIdx).itc,        Wavetoilet.subjects(subjIdx).event(eventIdx).itc(       currentIcIdx,:,:));
                icCluster(clusterIdx).event(eventIdx).PSD        = cat(1, icCluster(clusterIdx).event(eventIdx).PSD,        Wavetoilet.subjects(subjIdx).event(eventIdx).PSD(       currentIcIdx,:));
                icCluster(clusterIdx).event(eventIdx).subjIdx    = cat(1, icCluster(clusterIdx).event(eventIdx).subjIdx,    repmat(subjIdx, [length(currentIcIdx) 1]));
                icCluster(clusterIdx).event(eventIdx).subjName   = cat(1, icCluster(clusterIdx).event(eventIdx).subjName,   repmat({Wavetoilet.subjects(subjIdx).filename}, [length(currentIcIdx) 1]));
                icCluster(clusterIdx).event(eventIdx).icIdx      = cat(1, icCluster(clusterIdx).event(eventIdx).icIdx,      currentIcIdx);
                icCluster(clusterIdx).event(eventIdx).scalpTopo  = cat(1, icCluster(clusterIdx).event(eventIdx).scalpTopo,  Wavetoilet.subjects(subjIdx).scalpTopo(currentIcIdx,:,:));
                
                % Dipole position--this requires some processing.
                posxyz1D = [Wavetoilet.subjects(subjIdx).dipfitModels(currentIcIdx).posxyz];
                if length(posxyz1D)==3
                    posxyz2D = posxyz1D;
                else
                    posxyz2D = reshape(posxyz1D, [3, length(currentIcIdx)])';
                end
                icCluster(clusterIdx).event(eventIdx).posxyz     = cat(1, icCluster(clusterIdx).event(eventIdx).posxyz,     posxyz2D);
            end
        end
    end
end

% Update GUI report.
numIcSubjMatrix = zeros(length(icCluster),2);
for clusterIdx = 1:length(icCluster)
    numIcSubjMatrix(clusterIdx,:) = [length(icCluster(clusterIdx).event(1).icIdx) length(unique(icCluster(clusterIdx).event(1).subjIdx))];
end

numIcReport = get(handles.numIcsText, 'String');
numIcReport{2,1} = sprintf('Mean: %.1f', mean(numIcSubjMatrix(:,1),1));
numIcReport{3,1} = sprintf('SD: %.1f',   std(numIcSubjMatrix(:,1),1));
numIcReport{4,1} = sprintf('Range: %.0f-%.0f', min(numIcSubjMatrix(:,1)), max(numIcSubjMatrix(:,1)));
set(handles.numIcsText, 'String', numIcReport);

numuniqueSubjReport = get(handles.numUniqueSubjectsText, 'String');
numuniqueSubjReport{2,1} = sprintf('Mean: %.1f', mean(numIcSubjMatrix(:,2),1));
numuniqueSubjReport{3,1} = sprintf('SD: %.1f',   std(numIcSubjMatrix(:,2),1));
numuniqueSubjReport{4,1} = sprintf('Range: %.0f-%.0f', min(numIcSubjMatrix(:,2)), max(numIcSubjMatrix(:,2)));
set(handles.numUniqueSubjectsText, 'String', numuniqueSubjReport);

% Set the data
set(handles.performPushbutton, 'UserData', icCluster)
disp('The results are stored under handles.performPushbutton.UserData')



% --- Executes on button press in savePushbutton.
function savePushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to savePushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Load the processed data.
Wavetoilet = get(handles.performPushbutton, 'UserData');

% Obtain the save path and the file name.
[fileName, pathName] = uiputfile('.mat', 'Select path and enter output file name');

% Save the output.
savingPath = [pathName fileName];
save([savingPath(1:end-4) '_Wavetoilet2nd.mat'], 'Wavetoilet', '-v7.3', '-nocompression')



function numClustersEdit_Callback(hObject, eventdata, handles)
% hObject    handle to numClustersEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numClustersEdit as text
%        str2double(get(hObject,'String')) returns contents of numClustersEdit as a double


% --- Executes during object creation, after setting all properties.
function numClustersEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numClustersEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in methodPopupmenu.
function methodPopupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to methodPopupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns methodPopupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from methodPopupmenu


% --- Executes during object creation, after setting all properties.
function methodPopupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to methodPopupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
