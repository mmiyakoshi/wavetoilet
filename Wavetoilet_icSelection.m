% Wavetoilet_icSelection(varargin)
%
% History
% 12/18/2020 Makoto. Renamed to Wavetoilet.
% 11/29/2020 Makoto. Renamed to Fieldstrip.
% 11/20/2020 Makoto. Updated. The most dominant label supported.
% 11/08/2020 Makoto. Created.

% Copyright (C) 2020, Makoto Miyakoshi (mmiyakoshi@ucsd.edu) , SCCN,INC,UCSD
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
% THE POSSIBILITY OF SUCH DAMAGE.

function varargout = Wavetoilet_icSelection(varargin)
% WAVETOILET_ICSELECTION MATLAB code for Wavetoilet_icSelection.fig
%      WAVETOILET_ICSELECTION, by itself, creates a new WAVETOILET_ICSELECTION or raises the existing
%      singleton*.
%
%      H = WAVETOILET_ICSELECTION returns the handle to a new WAVETOILET_ICSELECTION or the handle to
%      the existing singleton*.
%
%      WAVETOILET_ICSELECTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WAVETOILET_ICSELECTION.M with the given input arguments.
%
%      WAVETOILET_ICSELECTION('Property','Value',...) creates a new WAVETOILET_ICSELECTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Wavetoilet_icSelection_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Wavetoilet_icSelection_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Wavetoilet_icSelection

% Last Modified by GUIDE v2.5 18-Dec-2020 04:32:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Wavetoilet_icSelection_OpeningFcn, ...
                   'gui_OutputFcn',  @Wavetoilet_icSelection_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Wavetoilet_icSelection is made visible.
function Wavetoilet_icSelection_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Wavetoilet_icSelection (see VARARGIN)

% Choose default command line output for Wavetoilet_icSelection
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Wavetoilet_icSelection wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Wavetoilet_icSelection_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function rvEdit_Callback(hObject, eventdata, handles)
% hObject    handle to rvEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rvEdit as text
%        str2num(get(hObject,'String')) returns contents of rvEdit as a double


% --- Executes during object creation, after setting all properties.
function rvEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rvEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function severeRejectionEdit_Callback(hObject, eventdata, handles)
% hObject    handle to severeRejectionEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of severeRejectionEdit as text
%        str2num(get(hObject,'String')) returns contents of severeRejectionEdit as a double


% --- Executes during object creation, after setting all properties.
function severeRejectionEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to severeRejectionEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function gentleRejectionEdit_Callback(hObject, eventdata, handles)
% hObject    handle to gentleRejectionEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gentleRejectionEdit as text
%        str2double(get(hObject,'String')) returns contents of gentleRejectionEdit as a double


% --- Executes during object creation, after setting all properties.
function gentleRejectionEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gentleRejectionEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in selectFilesButton.
function selectFilesButton_Callback(hObject, eventdata, handles)
% hObject    handle to selectFilesButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Obtain multiple .set files
[allFiles, workingFolder] = uigetfile('*.set', 'MultiSelect', 'on');
if ~any(workingFolder)
    disp('Cancelled.')
    return
end

% Convert char to cell if n = 1.
if ischar(allFiles)
    subjName = allFiles;
    clear allFiles
    allFiles{1,1} = subjName;    
end

% Display process start
disp(sprintf('\n'))
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%% ''1.IC selection'' started. %%%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(sprintf('\n'))

% Move to the working folder
cd(workingFolder)

% Start the batch
numOriginalIcs    = zeros(length(allFiles),1);
numNonartifactIcs = zeros(length(allFiles),1);
numBrainIcs       = zeros(length(allFiles),1);
for setIdx = 1:length(allFiles)
    loadName = allFiles{setIdx};
    dataName = loadName(1:end-4);
    
    %% STEP1: Load data.
    EEG = pop_loadset('filename', loadName, 'filepath', workingFolder, 'loadmode', 'info');

    if isfield(EEG.etc, 'Wavetoilet')
        EEG.etc = rmfield(EEG.etc, 'Wavetoilet');
        disp('Old results found; removed.')
    end
    
    %% STEP2: Find ICs associated with inside-brain dipoles.
    load(EEG.dipfit.hdmfile); % This returns 'vol'.
    dipoleXyz = zeros(length(EEG.dipfit.model),3);
    for icIdx = 1:length(EEG.dipfit.model)
        dipoleXyz(icIdx,:) = EEG.dipfit.model(icIdx).posxyz(1,:); % If symmetrical bilateral dipoles, select only one of them.
    end
    depth = ft_sourcedepth(dipoleXyz, vol);
    depthThreshold = 1;
    insideBrainIdx = find(depth<=depthThreshold);
    
    %% STEP3: Find ICs with less than threshold r.v..
    rvThreshold = str2num(get(handles.rvEdit, 'String'));
    rvList      = [EEG.dipfit.model.rv];
    goodRvIdx   = find(rvList < rvThreshold)'; % < 15% residual variance == good ICs.
    
    insideBrainAndGoodRvIdx = intersect(insideBrainIdx, goodRvIdx);
 
    %% STEP4: Identify artifact ICs. The order of the classes are {'Brain'  'Muscle'  'Eye'  'Heart'  'Line Noise'  'Channel Noise'  'Other'}.
    [~, mostDominantClassLabelVector] = max(EEG.etc.ic_classification.ICLabel.classifications, [], 2);
    mostDominantClassLabelProbVector = zeros(length(mostDominantClassLabelVector),1);
    for icIdx = 1:length(mostDominantClassLabelVector)
        mostDominantClassLabelProbVector(icIdx)  = EEG.etc.ic_classification.ICLabel.classifications(icIdx, mostDominantClassLabelVector(icIdx));
    end
    artifactLabelProbThresh = str2num(get(handles.gentleRejectionEdit, 'String'));
    artifactIcIdx = find((mostDominantClassLabelVector==2 & mostDominantClassLabelProbVector>=artifactLabelProbThresh)|...
                         (mostDominantClassLabelVector==3 & mostDominantClassLabelProbVector>=artifactLabelProbThresh)|...
                         (mostDominantClassLabelVector==4 & mostDominantClassLabelProbVector>=artifactLabelProbThresh)|...
                         (mostDominantClassLabelVector==5 & mostDominantClassLabelProbVector>=artifactLabelProbThresh)|...
                         (mostDominantClassLabelVector==6 & mostDominantClassLabelProbVector>=artifactLabelProbThresh));
    nonartifactIcIdx = setdiff(1:size(EEG.icaweights,1), artifactIcIdx);
    nonartifactGoodDipIcIdx = intersect(insideBrainAndGoodRvIdx, nonartifactIcIdx);
    EEG.etc.Wavetoilet.nonartifactIcIdx = nonartifactGoodDipIcIdx;

    %% STEP5: Identify brain ICs. The order of the classes are {'Brain'  'Muscle'  'Eye'  'Heart'  'Line Noise'  'Channel Noise'  'Other'}.
    brainLabelProbThresh  = str2num(get(handles.severeRejectionEdit, 'String'));
    brainIcIdx = find((mostDominantClassLabelVector==1 & mostDominantClassLabelProbVector>=brainLabelProbThresh));
    brainIcGoodDipIdx = intersect(insideBrainAndGoodRvIdx, brainIcIdx);
    EEG.etc.Wavetoilet.brainIcIdx = brainIcGoodDipIdx;    
    
    %% STEP6: Save data
    pop_saveset(EEG, 'filename', dataName, 'filepath', workingFolder);
    
    %% STEP7: Store the results.
    numOriginalIcs(setIdx,1)    = size(EEG.icaweights,1);
    numNonartifactIcs(setIdx,1) = length(nonartifactGoodDipIcIdx);
    numBrainIcs(setIdx,1)       = length(brainIcGoodDipIdx);
end


%% STEP8. Display stats.
disp(sprintf('\n\nNumber of ICs (original), M=%.1f (SD %.1f), range %.0f-%.0f.', ...
    mean(numOriginalIcs), std(numOriginalIcs), min(numOriginalIcs), max(numOriginalIcs)));
disp(sprintf('Number of ICs (Nonartifact ICs), M=%.1f (SD %.1f), range %.0f-%.0f.', ...
    mean(numNonartifactIcs), std(numNonartifactIcs), min(numNonartifactIcs), max(numNonartifactIcs)));
disp(sprintf('Number of ICs (Brain ICs), M=%.1f (SD %.1f), range %.0f-%.0f.', ...
    mean(numBrainIcs), std(numBrainIcs), min(numBrainIcs), max(numBrainIcs)));

%% STEP9. Export excel report.
subjNames   = allFiles';
outputTable = table(allFiles', numOriginalIcs, numNonartifactIcs, numBrainIcs, 'VariableNames', {'fileNames', 'numICsOriginal', 'numICsNonartifact', 'numICsBrain'});
writetable(outputTable, [workingFolder 'icSelectionSummary.xlsx'], 'WriteVariableNames', true, 'WriteRowNames', false);
disp(sprintf('Summary report was generated in %s', [workingFolder filesep 'icSelectionSummary.xlsx']))



% Display process end
disp(sprintf('\n'))
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%% ''1.IC selection'' finished. %%%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(sprintf('\n'))
