% Wavetoilet_structureGroupLevelData(varargin)
%
% History
% 12/18/2020 Makoto. Renamed to Wavetoilet.
% 12/14/2020 Makoto. Created.

function varargout = Wavetoilet_structureGroupLevelData(varargin)
% WAVETOILET_STRUCTUREGROUPLEVELDATA MATLAB code for Wavetoilet_structureGroupLevelData.fig
%      WAVETOILET_STRUCTUREGROUPLEVELDATA, by itself, creates a new WAVETOILET_STRUCTUREGROUPLEVELDATA or raises the existing
%      singleton*.
%
%      H = WAVETOILET_STRUCTUREGROUPLEVELDATA returns the handle to a new WAVETOILET_STRUCTUREGROUPLEVELDATA or the handle to
%      the existing singleton*.
%
%      WAVETOILET_STRUCTUREGROUPLEVELDATA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WAVETOILET_STRUCTUREGROUPLEVELDATA.M with the given input arguments.
%
%      WAVETOILET_STRUCTUREGROUPLEVELDATA('Property','Value',...) creates a new WAVETOILET_STRUCTUREGROUPLEVELDATA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Wavetoilet_structureGroupLevelData_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Wavetoilet_structureGroupLevelData_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Wavetoilet_structureGroupLevelData

% Last Modified by GUIDE v2.5 18-Dec-2020 04:35:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Wavetoilet_structureGroupLevelData_OpeningFcn, ...
                   'gui_OutputFcn',  @Wavetoilet_structureGroupLevelData_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Wavetoilet_structureGroupLevelData is made visible.
function Wavetoilet_structureGroupLevelData_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Wavetoilet_structureGroupLevelData (see VARARGIN)

% Choose default command line output for Wavetoilet_structureGroupLevelData
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Wavetoilet_structureGroupLevelData wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Wavetoilet_structureGroupLevelData_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on selection change in robustStatsPopupmenu.
function robustStatsPopupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to robustStatsPopupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns robustStatsPopupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from robustStatsPopupmenu

switch get(handles.robustStatsPopupmenu, 'Value')
    case 1
        set(handles.rhoText,       'Visible', 'off')
        set(handles.rhoEdit,       'Visible', 'off')
        set(handles.iterationText, 'Visible', 'off')
        set(handles.iterationEdit, 'Visible', 'off')
    case 2
        set(handles.rhoText,       'Visible', 'on')
        set(handles.rhoEdit,       'Visible', 'on')
        set(handles.iterationText, 'Visible', 'on')
        set(handles.iterationEdit, 'Visible', 'on')
end

% --- Executes during object creation, after setting all properties.
function robustStatsPopupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to robustStatsPopupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rhoEdit_Callback(hObject, eventdata, handles)
% hObject    handle to rhoEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rhoEdit as text
%        str2double(get(hObject,'String')) returns contents of rhoEdit as a double


% --- Executes during object creation, after setting all properties.
function rhoEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rhoEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function iterationEdit_Callback(hObject, eventdata, handles)
% hObject    handle to iterationEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of iterationEdit as text
%        str2double(get(hObject,'String')) returns contents of iterationEdit as a double


% --- Executes during object creation, after setting all properties.
function iterationEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to iterationEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function savingPathEdit_Callback(hObject, eventdata, handles)
% hObject    handle to savingPathEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of savingPathEdit as text
%        str2double(get(hObject,'String')) returns contents of savingPathEdit as a double


% --- Executes during object creation, after setting all properties.
function savingPathEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to savingPathEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectTheFolderPushbutton.
function selectTheFolderPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to selectTheFolderPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Set the save path and the file name.
[fileName, pathName] = uiputfile('.mat', 'Select path and enter output file name');

% Set the fullpath to the edit box.
set(handles.savingPathEdit, 'string', [pathName fileName]);



% --- Executes on button press in selectSetFilesPushbutton.
function selectSetFilesPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to selectSetFilesPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

disp(sprintf('\n'))
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%% ''3. Structure group-level data'' started. %%%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(sprintf('\n'))

% Add path to Christian's robust_mean function.
fullpathToThisFile = mfilename('fullpath');
pathToWaveletFunctions = [fullpathToThisFile(1:end-35) filesep 'external' filesep 'christiansCodeFromBCILAB'];
addpath(pathToWaveletFunctions);
existFcwt = exist('robust_mean');
if existFcwt==0
    error('Please check the path to [eeglabroot]/plugins/Wavetoilet/external/christiansCodeFromBCILAB')
end

% Obtain multiple .set files
[allFiles, workingFolder] = uigetfile('*.set', 'MultiSelect', 'on');
if ~any(workingFolder)
    disp('Cancelled.')
    return
end

% Convert char to cell if n = 1.
if ischar(allFiles)
    subjName = allFiles;
    clear allFiles
    allFiles{1,1} = subjName;    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Obtain total number of ICs. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numIcList     = zeros(length(allFiles),1);
for setIdx = 1:length(allFiles)
    loadName = allFiles{setIdx};
    dataName = loadName(1:end-4);
    
    % Load data.
    EEG = pop_loadset('filename', loadName, 'filepath', workingFolder, 'loadmode', 'info');

    % Obtain number of ICs and data legnth.
    switch EEG.etc.Wavetoilet.icSelection
        case 1
            numICs = size(EEG.icaweights,1);
        case 2
            numICs = length(EEG.etc.Wavetoilet.nonartifactIcIdx);
        case 3
            numICs = length(EEG.etc.Wavetoilet.brainIcIdx);
    end
    numIcList(setIdx,1) = numICs;
end

% If Huber mean, provide parameters.
if get(handles.robustStatsPopupmenu, 'Value')==2
    rho = str2num(get(handles.rhoEdit, 'String'));
    numIterations = str2num(get(handles.iterationEdit, 'String'));
end

% Prepare the progress bar.
processingTimePerSetList = zeros(length(allFiles),1);
numIcsProcessed = 0;
waitbarHandle = waitbar(0, 'Estimating the ramining processing time...');
set(waitbarHandle, 'Color', [0.66 0.76 1]);
drawnow

for setIdx = 1:length(allFiles)
    loadName = allFiles{setIdx};
    dataName = loadName(1:end-4);
    
    % Measure the time lapse.
    tStart = tic;
    
    %% STEP1: Load data.
    EEG = pop_loadset('filename', loadName, 'filepath', workingFolder, 'loadmode', 'info');

    %% STEP2: Return error if precompute result not present
    if ~isfield(EEG.etc, 'Wavetoilet')
        error('Fieldstrip precompute result not present.')
    end

    %% STEP3: Calculate ERP baseline values.
    baseErp = EEG.etc.Wavetoilet.erpBase;
    baseErpMean      = mean(mean(baseErp,3),2);
    switch get(handles.robustStatsPopupmenu, 'Value')
        case 1
            baseErpRobust = median(median(baseErp,3),2);
        case 2
            baseErpRobust = robust_mean(baseErp(:,:)', rho, numIterations)';
    end
    
    %% STEP4: Calculate ERSP baseline values.
    tic
    baseWtCoeff  = EEG.etc.Wavetoilet.wtCoeffBase;
    baseErsp     = 10*log10(baseWtCoeff.*conj(baseWtCoeff));
    baseErspMean = mean(baseErsp(:,:,:),3);
    switch get(handles.robustStatsPopupmenu, 'Value')
        case 1
            baseErspRobust = median(baseErsp(:,:,:),3);
        case 2
            baseErsp_dim12_34_2D = reshape(baseErsp, [size(baseErsp,1)*size(baseErsp,2) size(baseErsp,3)*size(baseErsp,4)]);
            baseErspRobust_1D    = robust_mean(baseErsp_dim12_34_2D', rho, numIterations)';
            baseErspRobust      = reshape(baseErspRobust_1D, [size(baseErsp,1) size(baseErsp,2)]);
            clear baseErsp_dim12_34_2D baseErspRobust_1D
    end

    %% STEP5: Loop for all events.
    latencyZeroEvents = EEG.etc.Wavetoilet.latencyZeroEvents;
    for uniqueEventIdx = 1:length(latencyZeroEvents)
        currentEvent = latencyZeroEvents(uniqueEventIdx);
        currentEventIdx = find(contains(EEG.etc.Wavetoilet.eventList, currentEvent));
        
        %% STEP6: Calculate ERPs.
        currentErp = EEG.etc.Wavetoilet.erp(:,:,currentEventIdx);
        meanErp      = bsxfun(@minus, mean(currentErp,3),  baseErpMean);
        switch get(handles.robustStatsPopupmenu, 'Value')
            case 1
                robustErp = bsxfun(@minus, median(currentErp,3), baseErpRobust);
            case 2
                currentErp_dim312  = permute(currentErp, [3 1 2]);
                robustErp_dim12_1D = robust_mean(currentErp_dim312(:,:));
                robustErp_dim12_2D = reshape(robustErp_dim12_1D, size(meanErp));
                robustErp = bsxfun(@minus, robustErp_dim12_2D, baseErpRobust);
        end
        
        %% STEP7: Calculate ERSP.
        currentCoeff = EEG.etc.Wavetoilet.wtCoeff(:,:,:,currentEventIdx);
        currentErsp  = 10*log10(currentCoeff.*conj(currentCoeff));
        meanErsp     = bsxfun(@minus, mean(currentErsp,4), baseErspMean);
        switch get(handles.robustStatsPopupmenu, 'Value')
            case 1
                robustErsp = bsxfun(@minus, median(currentErsp,4), baseErspRobust);
            case 2
                currentErsp_dim123_4_2D = reshape(currentErsp, [size(currentErsp,1)*size(currentErsp,2)*size(currentErsp,3) size(currentErsp,4)]);
                currentErspRobust_1D    = robust_mean(currentErsp_dim123_4_2D', rho, numIterations)';
                currentErspRobust       = reshape(currentErspRobust_1D, [size(currentErsp,1) size(currentErsp,2) size(currentErsp,3)]);
                robustErsp              = bsxfun(@minus, currentErspRobust, baseErspRobust);
                clear currentErsp_dim123_4_2D currentErspRobust_1D currentErspRobust
        end

        %% STEP8: Calculate ITC.
        normFactorCoeff = sqrt(sum(currentCoeff.*conj(currentCoeff),4)*size(currentCoeff,4));
        itc  = abs(sum(bsxfun(@rdivide, currentCoeff, normFactorCoeff),4));

        %% STEP9: Calculate PSD.
        [PSD, psdFreqs] = spectopo(currentErp, size(currentErp,2), EEG.srate, 'freqfac', 4, 'overlap', round(EEG.srate/2), 'plot', 'off');
        
        %% STEP10: Store the event-dependent results.
        Wavetoilet.subjects(setIdx).event(uniqueEventIdx).label      = currentEvent;
        Wavetoilet.subjects(setIdx).event(uniqueEventIdx).meanErp    = meanErp;
        Wavetoilet.subjects(setIdx).event(uniqueEventIdx).robustErp  = robustErp;
        Wavetoilet.subjects(setIdx).event(uniqueEventIdx).meanErsp   = meanErsp;
        Wavetoilet.subjects(setIdx).event(uniqueEventIdx).robustErsp = robustErsp;
        Wavetoilet.subjects(setIdx).event(uniqueEventIdx).itc        = itc;
        Wavetoilet.subjects(setIdx).event(uniqueEventIdx).PSD        = PSD;
    end
    
    %% STEP11: Store the non-event-dependent results.
    Wavetoilet.subjects(setIdx).setname   = EEG.setname;
    Wavetoilet.subjects(setIdx).filename  = EEG.filename;
    Wavetoilet.subjects(setIdx).filepath  = EEG.filepath;
    Wavetoilet.subjects(setIdx).subject   = EEG.subject;
    Wavetoilet.subjects(setIdx).group     = EEG.group;
    Wavetoilet.subjects(setIdx).condition = EEG.condition;
    Wavetoilet.subjects(setIdx).session   = EEG.session;
    switch EEG.etc.Wavetoilet.icSelection
        case 1
            Wavetoilet.subjects(setIdx).icIdx = single(size(EEG.icaweights,1));
        case 2
            Wavetoilet.subjects(setIdx).icIdx = single(EEG.etc.Wavetoilet.nonartifactIcIdx);
        case 3
            Wavetoilet.subjects(setIdx).icIdx = single(EEG.etc.Wavetoilet.brainIcIdx);
    end
    Wavetoilet.subjects(setIdx).scalpTopo    = EEG.etc.Wavetoilet.scalpTopo;
    Wavetoilet.subjects(setIdx).epochTime    = single(EEG.etc.Wavetoilet.epochTime);
    Wavetoilet.subjects(setIdx).baselineTime = single(EEG.etc.Wavetoilet.baselineTime);
    Wavetoilet.subjects(setIdx).wtFreqs      = single(EEG.etc.Wavetoilet.wtFreqs);
    Wavetoilet.subjects(setIdx).psdFreqs     = single(psdFreqs);
    Wavetoilet.subjects(setIdx).dipfitModels = EEG.dipfit.model(Wavetoilet.subjects(setIdx).icIdx);
    
    
    %% STEP12: Estimate processing time.
    tElapsed = toc(tStart);
    numIcsProcessed = numIcsProcessed + numIcList(setIdx);
    processingTimePerSetList(setIdx) = tElapsed; % s
    averageSpeedPerIc = sum(processingTimePerSetList(1:setIdx))/numIcsProcessed;
    numRemainingIcs = sum(numIcList)-numIcsProcessed;
    remainingTime = averageSpeedPerIc*numRemainingIcs;
    percentFinished = 100*numIcsProcessed/sum(numIcList);
    if     remainingTime >= 86400
        waitbarMessage = sprintf('\n\n%.0f%% done. Remaining %.1f days.',    percentFinished, remainingTime/86400);
    elseif remainingTime >= 3600
        waitbarMessage = sprintf('\n\n%.0f%% done. Remaining %.1f hours.',   percentFinished, remainingTime/3600);
    else
        waitbarMessage = sprintf('\n\n%.0f%% done. Remaining %.1f minutes.', percentFinished, remainingTime/60);
    end
    waitbarHandle = waitbar(percentFinished/100, waitbarHandle, waitbarMessage);
    %         colorbarHandle = findobj(waitbarHandle,'Type','Patch');
    %         set(colorbarHandle, 'EdgeColor',[0 0 0], 'FaceColor',[1 0.66 0.76])
    set(waitbarHandle, 'Color', [0.66 0.76 1]);
    drawnow
end
close(waitbarHandle)

% Save the data.
savingPath = get(handles.savingPathEdit, 'string');
save([savingPath(1:end-4) '_Wavetoilet1st.mat'], 'Wavetoilet', '-v7.3', '-nocompression')

% Display process end
disp(sprintf('\n'))
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%% ''3. Structure group-level data'' finished. %%%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(sprintf('\n'))
